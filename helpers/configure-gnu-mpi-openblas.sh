#!/bin/sh

# GNU - OpenMPI - OpenBLAS

../configure \
    MPICC="mpicc" \
    MPICXX="mpic++" \
    MPIFC="mpifort" \
    LINALG_LIBS="-lscalapack${NETLIB_SUFFIX} -lopenblas" \
    "$@"
