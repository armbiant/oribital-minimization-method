#!/bin/sh

# GNU - Serial - Netlib

../configure \
    --without-mpi \
    CC="gcc" \
    CXX="g++" \
    FC="gfortran" \
    LINALG_LIBS="-llapack -lblas" \
    "$@"
